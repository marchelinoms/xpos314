﻿using Microsoft.AspNetCore.Mvc;
using xpos314.web.Models;
namespace xpos314.web.Controllers
{
    public class FriendController : Controller
    {
        private static List<Friend> friends = new List<Friend>()
     {
        new Friend() { Id = 1 , Name = "Anwar", Address = "Jakarta" },
        new Friend() { Id = 2 , Name = "Asti", Address = "Garut" }
     };

        public IActionResult Index()
        {

            ViewBag.listFriend = friends;

            return View(friends);
        }
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(Friend friend)
        {
            friends.Add(friend);
            return RedirectToAction("Index");
        }
        //public IActionResult Edit(int id)
        //{
        //    Friend editFriend = friends.Find(i => i.Id == id)!;

        //    return View(editFriend);
        //}
        //[HttpPost]
        //public IActionResult Edit(Friend data)
        //{
        //    Friend friend1 = friends.Find(i => i.Id == data.Id)!;
        //    int index = friends.IndexOf(friend1);

        //    if (index > -1)
        //    {
        //        friends[index].Id = data.Id;
        //        friends[index].Name = data.Name;
        //        friends[index].Address = data.Address;
        //    }
        //    return RedirectToAction("Index");
        //}
        [HttpGet]
        [HttpPost]
        public IActionResult Edit(Friend data) 
        {
            Friend friend1 = friends.Find(i => i.Id == data.Id)!;
            int index = friends.IndexOf(data);
            if (HttpContext.Request.Method == "POST")
            {
                if (index > -1)
                {
                    friends[index].Id = data.Id;
                    friends[index].Name = data.Name;
                    friends[index].Address = data.Address;
                }
                return RedirectToAction("Index");
            }
            else
                return View(data);
        }
        public IActionResult Detail(int id)
        {
            Friend detailFriend = friends.Find(i => i.Id == id)!;

            return View(detailFriend);
        }
        //public IActionResult Delete(int id) 
        //{

        //    Friend deleteFriend = friends.Find(i => i.Id == id)!;

        //    return View(deleteFriend);
        //}
        //[HttpPost]
        //public IActionResult Delete(String id) 
        //{
        //    Friend deleteFriend = friends.Find(i => i.Id == int.Parse(id))!;
        //    friends.Remove(deleteFriend);
        //    return RedirectToAction("Index");
        //}
        [HttpGet]
        [HttpPost]
        public IActionResult Delete(int id) 
        {
            Friend data = friends.Find(i => i.Id == id)!;
            if (HttpContext.Request.Method == "POST")
            {
                friends.Remove(data);
                return RedirectToAction("Index");
            }
            else
                return View(data);
        }
    }
}
