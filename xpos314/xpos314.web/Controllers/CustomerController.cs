﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CustomerController : Controller
    {
        private CustomerService customerService;
        private RoleService roleService;
        private int idUser = 1;

        public CustomerController(CustomerService customerService, RoleService roleService)
        {
            this.customerService = customerService;
            this.roleService = roleService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<VMTblCustomer> datas = await customerService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                datas = datas.Where(d => d.NameCustomer.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.NameCustomer).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.NameCustomer).ToList();
                    break;

            }
            return View(PaginatedList<VMTblCustomer>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<IActionResult> Create()
        {
            VMTblCustomer data = new VMTblCustomer();
            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(VMTblCustomer dataParam)
        {
            VMResponse response = await customerService.Create(dataParam);

            if (response.Success)
                return Json(new { dataRespon = response });

            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            List<TblRole> listRole = await roleService.GetAllData();
            ViewBag.ListRole = listRole;
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        {
            VMResponse response = await customerService.Edit(dataParam);
            dataParam.UpdateBy = idUser;
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id)
        {
            VMResponse response = await customerService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<IActionResult> Restore(int id)
        {
            VMTblCustomer data = await customerService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Restored(int id)
        {
            VMResponse response = await customerService.Restore(id);
            if (response.Success)
                return RedirectToAction("Index");
            return RedirectToAction("Index", id);
        }
    }
}
