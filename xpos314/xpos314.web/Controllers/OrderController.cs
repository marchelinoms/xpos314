﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
	public class OrderController : Controller
	{
        private ProductService productService;
        private OrderService   orderService;
		private int idUser = 1;
		public OrderController(ProductService _productService, OrderService _orderService)
		{
			this.productService = _productService;
			this.orderService = _orderService;
		}
		public async Task<IActionResult> Catalog(VMSearchPage dataSearch) 
		{
			List<VMTblProduct> product = await productService.GetAllData();
			dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
			dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;

			if(dataSearch.NameProduct != null)
				product = product.Where(a => a.NameProduct == dataSearch.NameProduct).ToList();

			if(dataSearch.MinAmount != null && dataSearch.MaxAmount != null)
				product = product.Where(a => a.Price >= dataSearch.MinAmount && a.Price <= dataSearch.MaxAmount).ToList();
			
			//Set Session in VMOrderHeader first load
			VMOrderHeader dataHeader = HttpContext.Session.GetComplexData<VMOrderHeader>("ListCart");
			if (dataHeader == null) 
			{
				dataHeader = new VMOrderHeader();
				dataHeader.ListDetails = new List<VMOrderDetail>();
			}

			var ListDetail = JsonConvert.SerializeObject(dataHeader.ListDetails);
			ViewBag.dataHeader = dataHeader;
			ViewBag.dataDetail = ListDetail;
			//End Session

			ViewBag.Search = dataSearch;
			ViewBag.CurrentPageSize = dataSearch.CurrentPageSize;

			return View(PaginatedList<VMTblProduct>.CreateAsync(product,dataSearch.pageNumber ?? 1 , dataSearch.pageSize ?? 3));
		}
		//Set Session in VMOrderHeader every add or minus
		[HttpPost]
		public JsonResult SetSession(VMOrderHeader dataHeader) 
		{
			HttpContext.Session.SetComplexData("ListCart", dataHeader);
			return Json("");
		}
		//End Session
		public JsonResult EndSession() 
		{
			HttpContext.Session.Remove("ListCart");
			return Json("");
		}
		public IActionResult OrderPreview(VMOrderHeader dataHeader) 
		{
			return PartialView(dataHeader);
		}
		public IActionResult SearchMenu() 
		{
			return PartialView();
		}

		[HttpPost]
		public async Task<JsonResult> SubmitOrder(VMOrderHeader dataHeader) 
		{
			dataHeader.IdCustomer = idUser;
		
			VMResponse response = await orderService.SubmitOrder(dataHeader);
			return Json(response);
		}
		public async Task<IActionResult> HistoryOrder(VMSearchPage dataSearch) 
		{
			int IdCustomer = idUser;
			List<VMOrderHeader> data = await orderService.ListHeaderDetails(IdCustomer);
			int count = await orderService.CountTransaction(IdCustomer);
			ViewBag.Count = count;
           
			dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;
			
			dataSearch.MinDate = dataSearch.MinDate == null ? DateTime.MinValue : dataSearch.MinDate;
            dataSearch.MaxDate = dataSearch.MaxDate == null ? DateTime.MaxValue : dataSearch.MaxDate;

			if (!string.IsNullOrEmpty(dataSearch.CodeTransaction))
			{
				data = data.Where(a => a.CodeTransaction.ToLower()
				.Contains(dataSearch.CodeTransaction.ToLower())).ToList();
			}
			if (dataSearch.MinDate != null && dataSearch.MaxDate != null) 
			{
				data = data.Where(a => a.CreateDate >= dataSearch.MinDate && 
				a.CreateDate <= dataSearch.MaxDate).ToList();
			}
			if (dataSearch.MinAmount != null && dataSearch.MaxAmount != null) 
			{
				data = data.Where(a => a.Amount >= dataSearch.MinAmount && 
				a.Amount <= dataSearch.MaxAmount).ToList();
			}
			ViewBag.Search = dataSearch;

			return View(data);
        }
		public IActionResult Search() 
		{
			return PartialView();
		}
    }
}
