﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class ProductController : Controller
    {
        private CategoryService categoryService;
        private VariantService variantService;
        private ProductService productService;
        private int idUser = 1;
        private readonly IWebHostEnvironment webHostEnvironment;
        public ProductController(ProductService _productService,VariantService _variantService, CategoryService _categoryService,IWebHostEnvironment _webhostEnvironment)
        {
            this.productService = _productService;
            this.categoryService = _categoryService;
            this.variantService = _variantService;
            this.webHostEnvironment = _webhostEnvironment;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSort = sortOrder == "price" ? "price_desc" : "price";
            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<VMTblProduct> datas = await productService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                datas = datas.Where(d => d.NameProduct.ToLower().Contains(searchString.ToLower())
                || d.NameVariant.ToLower().Contains(searchString.ToLower())
                || d.NameCategory.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.NameProduct).ToList();
                    break;
                case "price_desc":
                    datas = datas.OrderByDescending(d => d.Price).ToList();
                    break;
                case "price":
                    datas = datas.OrderBy(d => d.Price).ToList();
                    break;
                default:
                    datas = datas.OrderBy(d => d.NameProduct).ToList();
                    break;

            }
            return View(PaginatedList<VMTblProduct>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<JsonResult> GetDataByIdCategory(int id) 
        {
            List<VMTblVariant> data = await variantService.GetDataByIdCategory(id);
            return Json(data);
        }
        public string Upload(VMTblProduct dataParam) 
        {
            string uniqueFileName = "";

            if (dataParam.ImageFile != null) 
            {
                string uploadFolder = Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + dataParam.ImageFile.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create)) 
                {
                    dataParam.ImageFile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }
        public async Task<IActionResult> Create()
        {
            VMTblProduct data = new VMTblProduct();
            List<TblCategory> listCategory = await categoryService.GetAllData();
            List<VMTblVariant> listVariant = await variantService.GetAllData();
            ViewBag.ListCategory = listCategory;
            ViewBag.ListVariant = listVariant;
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create([FromForm]VMTblProduct dataParam)
        {
            if(dataParam.ImageFile != null)
                dataParam.Image = Upload(dataParam);

            VMResponse response = await productService.Create(dataParam);

            if (response.Success)
                return Json(new { dataRespon = response });

            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id) 
        {
            VMTblProduct data = await productService.GetDataById(id);

            List<TblCategory> listCategory = await categoryService.GetAllData();
            List<VMTblVariant> listVariant = await variantService.GetAllData();
            ViewBag.ListCategory = listCategory;
            ViewBag.ListVariant = listVariant;

            return PartialView(data);
        }
        [HttpPut]
        public async Task<IActionResult> Edit([FromForm]VMTblProduct dataParam) 
        {
            if (dataParam.ImageFile != null)
                dataParam.Image = Upload(dataParam);

            VMResponse response = await productService.Edit(dataParam);

            if (response.Success)
                return Json(new { dataRespon = response });

            return View(dataParam);
        }
        public async Task<IActionResult> Detail(int id)
        {
            VMTblProduct data = await productService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<IActionResult> Delete(int id)
        {
            VMTblProduct data = await productService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id)
        {
            VMResponse response = await productService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
    }
}
