﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class VariantTryController : Controller
    {
        private readonly XPOS_314Context db;
        private readonly VariantTryService variantTryService;
        private readonly CategoryTryService categoryTryService;

        private static VMPage page = new VMPage();

        public VariantTryController(XPOS_314Context _db)
        {
            this.db = _db;
            this.variantTryService = new VariantTryService(db);
            this.categoryTryService = new CategoryTryService(db);
        }

        public IActionResult Index(VMPage pg)
        {
            ViewBag.idSort = string.IsNullOrEmpty(pg.sortOrder) ? "id_desc" : "";
            ViewBag.nameSort = pg.sortOrder == "name" ? "name_desc" : "name";
            ViewBag.currentSort = pg.sortOrder;
            ViewBag.currentShowData = pg.showData;

            if (pg.searchString != null)
                pg.pageNumber = 1;
            else
                pg.searchString = pg.currentFilter;

            ViewBag.currentFilter = pg.searchString;

            List<VMTblVariant> dataView = variantTryService.GetAllData();

            if (!string.IsNullOrEmpty(pg.sortOrder))
            {
                dataView = dataView.Where(i => i.NameVariant.ToLower().Contains(pg.searchString.ToLower()) || i.NameCategory.ToLower().Contains(pg.searchString.ToLower())).ToList();
            }
            switch (pg.sortOrder)
            {
                case "name_desc":
                    dataView = dataView.OrderByDescending(a => a.NameVariant).ToList();
                    break;
                case "name":
                    dataView = dataView.OrderBy(a => a.NameVariant).ToList();
                    break;
                case "id_desc":
                    dataView = dataView.OrderByDescending(a => a.Id).ToList();
                    break;
                default:
                    dataView = dataView.OrderBy(a => a.Id).ToList();
                    break;
            }

            int pgSize = pg.showData ?? 3;
            page = pg;
            return View(PaginatedList<VMTblVariant>.CreateAsync(dataView, pg.pageNumber ?? 1, pgSize));
        }
        public IActionResult Create()
        {
            VMTblVariant dataView = new VMTblVariant();
            ViewBag.DropDownCategory = categoryTryService.GetAllData();
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Create(VMTblVariant dataView)
        {
            VMResponse response = new VMResponse();
            if (ModelState.IsValid)
            {
                response = variantTryService.Create(dataView);

                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewBag.DropDownCategory = categoryTryService.GetAllData();
            response.Entity = dataView;
            return View(response.Entity);
        }
        public IActionResult Edit(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            ViewBag.DropDownCategory = categoryTryService.GetAllData();
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Edit(VMTblVariant dataView) 
        {
            VMResponse response = new VMResponse();

            if (ModelState.IsValid) 
            {
                response = variantTryService.Edit(dataView);
                if (response.Success) 
                {
                    return RedirectToAction("Index");
                }
            }
            ViewBag.DropDownCategory = categoryTryService.GetAllData();
            response.Entity = dataView;
            return View(dataView);
        }
        public IActionResult Detail(int id) 
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            return View(dataView);
        }
        public IActionResult Delete(int id)
        {
            VMTblVariant dataView = variantTryService.GetById(id);
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Delete(VMTblVariant dataView)
        {
            VMResponse response = variantTryService.Delete(dataView);
            if (response.Success)
            {
                return RedirectToAction("Index");
            }

            return View(response.Entity);
        }
    }
}
