﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CategoryTryController : Controller
    {
        private readonly CategoryTryService categoryTryService;
        private readonly XPOS_314Context db;

        public CategoryTryController(XPOS_314Context _db)
        {
            this.db = _db;
            this.categoryTryService = new CategoryTryService(db);
        }
        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryTryService.GetAllData();
            return View(dataView);
        }
        public IActionResult Create() 
        {
            VMTblCategory dataView = new VMTblCategory();
            return View(dataView);
        }

        [HttpPost]
        public IActionResult Create(VMTblCategory dataView) 
        {
            VMResponse response = new VMResponse();
            if (ModelState.IsValid) 
            {
                response = categoryTryService.Create(dataView);

                if (response.Success) 
                { 
                    return RedirectToAction("Index");
                }
            }
            response.Entity = dataView;
            return View(response.Entity);
        }
        public IActionResult Edit(int id) 
        {
            VMTblCategory dataView = categoryTryService.GetId(id);
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Edit(VMTblCategory dataview) 
        {
            VMResponse response = new VMResponse();
            if (ModelState.IsValid)
            {
                response = categoryTryService.Edit(dataview);

                if (response.Success) 
                {
                    return RedirectToAction("Index");
                }
            }
            response.Entity = dataview;
            return View(response.Entity);
        }
        public IActionResult Detail(int id) 
        {
            VMTblCategory dataView = categoryTryService.GetId(id);
            
            return View(dataView);
        }
        public IActionResult Delete(int id) 
        {
            VMTblCategory dataView = categoryTryService.GetId(id);
            return View(dataView);
        }
        [HttpPost]
        public IActionResult Delete(VMTblCategory dataView) 
        {
            VMResponse response = categoryTryService.Delete(dataView);
            if (response.Success) 
            {
                return RedirectToAction("Index");
            }

            return View(response.Entity);
        }
    }
}
