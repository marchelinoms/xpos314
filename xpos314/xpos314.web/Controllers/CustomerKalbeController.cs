﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CustomerKalbeController : Controller
    {
        private CustomerKalbeService customerKalbeService;

        public CustomerKalbeController(CustomerKalbeService _customerKalbeService)
        {
            this.customerKalbeService = _customerKalbeService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<CustomerKalbe> datas = await customerKalbeService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                datas = datas.Where(d => d.CustomerName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.CustomerName).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.CustomerName).ToList();
                    break;

            }
            return View(PaginatedList<CustomerKalbe>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        //public async Task<IActionResult> Create()
        //{
        //    VMTblCustomer data = new VMTblCustomer();
        //    //CustomerKalbe listGender = await customerKalbeService.GetGender();
        //    ViewBag.ListGender = listGender;
        //    return PartialView(data);
        //}
        //[HttpPost]
        //public async Task<IActionResult> Create(VMTblCustomer dataParam)
        //{
            //VMResponse response = await customerKalbeService.Create(dataParam);

            //if (response.Success)
            //    return Json(new { dataRespon = response });

        //    return View(dataParam);
        //}
        //public async Task<IActionResult> Edit(int id)
        //{
            //VMTblCustomer data = await customerKalbeService.GetDataById(id);
            //CustomerKalbe listGender = await customerKalbeService.GetGender();
            //ViewBag.ListGender = listGender;
            //return PartialView(data);
        //}
        //[HttpPost]
        //public async Task<IActionResult> Edit(VMTblCustomer dataParam)
        //{
        //    VMResponse response = await customerKalbeService.Edit(dataParam);
        //    if (response.Success)
        //        return Json(new { dataRespon = response });
        //    return View(dataParam);
        //}
        //public async Task<IActionResult> Delete(int id)
        //{
        //    CustomerKalbe data = await customerKalbeService.GetDataById(id);
        //    return PartialView(data);
        //}
        //[HttpPost]
        //public async Task<IActionResult> Deleted(int id)
        //{
        //    VMResponse response = await customerKalbeService.Delete(id);
        //    if (response.Success)
        //        return RedirectToAction("Index");

        //    return RedirectToAction("Index", id);
        //}
        //public async Task<IActionResult> Detail(int id)
        //{
        //    CustomerKalbe data = await customerKalbeService.GetDataById(id);
        //    return PartialView(data);
        //}
        //public async Task<IActionResult> Restore(int id)
        //{
        //    CustomerKalbe data = await customerKalbeService.GetDataById(id);
        //    return PartialView(data);
        //}
        //[HttpPost]
        //public async Task<IActionResult> Restored(int id)
        //{
        //    //VMResponse response = await customerKalbeService.Restore(id);
        //    if (response.Success)
        //        return RedirectToAction("Index");
        //    return RedirectToAction("Index", id);
        //}
    }
}
