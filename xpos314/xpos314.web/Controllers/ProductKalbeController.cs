﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class ProductKalbeController : Controller
    {
        private ProductKalbeService productKalbeService;
        public ProductKalbeController(ProductKalbeService _productKalbeService)
        {
            this.productKalbeService = _productKalbeService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<ProductKalbe> datas = await productKalbeService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                datas = datas.Where(d => d.ProductName.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.ProductName).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.ProductName).ToList();
                    break;

            }

            return View(PaginatedList<ProductKalbe>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        public IActionResult Create()
        {
            ProductKalbe data = new ProductKalbe();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(ProductKalbe dataParam)
        {
            VMResponse response = await productKalbeService.Create(dataParam);
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<JsonResult> CheckNameIsExist(string productName)
        {
            bool isExist = await productKalbeService.CheckProductByName(productName);
            return Json(isExist);
        }
        public async Task<IActionResult> Edit(int id)
        {
            ProductKalbe data = await productKalbeService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(ProductKalbe dataParam)
        {
            
            VMResponse response = await productKalbeService.Edit(dataParam);
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            ProductKalbe data = await productKalbeService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id)
        {
            VMResponse response = await productKalbeService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Restore(int id)
        {
            ProductKalbe data = await productKalbeService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Restored(int id)
        {
            VMResponse response = await productKalbeService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        
        public async Task<IActionResult> Detail(int id)
        {
            ProductKalbe data = await productKalbeService.GetDataById(id);
            return PartialView(data);
        }
    }
}
