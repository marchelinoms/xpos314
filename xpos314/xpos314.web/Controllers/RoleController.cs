﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class RoleController : Controller
    {
        private RoleService roleService;
        private int idUser = 1;
        public RoleController(RoleService _roleService)
        {
            this.roleService = _roleService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<TblRole> datas = await roleService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
                datas = datas.Where(d => d.RoleName!.ToLower().Contains(searchString.ToLower())).ToList();
            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.RoleName).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.RoleName).ToList();
                    break;

            }
            return View(PaginatedList<TblRole>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        public IActionResult Create()
        {
            TblRole data = new TblRole();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(TblRole dataParam)
        {
            dataParam.CreateBy = idUser;
            VMResponse response = await roleService.Create(dataParam);
            if (response.Success) 
            {
                return Json(new { dataRespon = response });
            }
            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(TblRole dataParam)
        {
            dataParam.UpdateBy = idUser;
            VMResponse response = await roleService.Edit(dataParam);
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id)
        {
            VMResponse response = await roleService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Restore(int id) 
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Restored(int id) 
        {
            VMResponse response = await roleService.Restore(id);
            if (response.Success)
                return RedirectToAction("Index");
            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            TblRole data = await roleService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<JsonResult> CheckNameIsExist(string roleName)
        {
            bool isExist = await roleService.CheckRoleByName(roleName);
            return Json(isExist);
        }
    }
}
