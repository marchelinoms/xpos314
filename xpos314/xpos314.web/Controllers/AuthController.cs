﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;
        VMResponse response = new VMResponse();
        public AuthController(AuthService _authService)
        {
            this.authService = _authService;
        }
        public IActionResult Login()
        {
            return PartialView();
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        public async Task<JsonResult> LoginSubmit(string email, string password)
        {
            VMTblCustomer customer = await authService.CheckLogin(email, password);
            if (customer != null)
            {
                response.Message = $"Hello, {customer.NameCustomer} Welcome to XPOS 314";
                HttpContext.Session.SetString("NameCustomer", customer.NameCustomer);
                HttpContext.Session.SetInt32("IdCustomer", customer.Id);
                HttpContext.Session.SetInt32("IdRole", customer.IdRole ?? 0);
            }
            else
                response.Message = $"Oops, {email} not found or incorrect password, please check again!";

            return Json(new { dataRespon = response });
        }
        public IActionResult Register()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<JsonResult> Register(TblCustomer customer)
        {
            response = await authService.CreateCustomer(customer);
            return Json(new { dataResponse = response });
        }
        public async Task<JsonResult> CheckEmailExisting(string email)
        {
            bool isExist = await authService.CheckEmailExisting(email);
            return Json(isExist);
        }
    }
}
