﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
	public class VariantController : Controller
	{
        private CategoryService categoryService;
        private VariantService variantService;
        private int idUser = 1;

        public VariantController(VariantService _variantService, CategoryService _categoryService)
        {
            this.categoryService = _categoryService;
            this.variantService = _variantService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<VMTblVariant> datas = await variantService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                datas = datas.Where(d => d.NameVariant.ToLower().Contains(searchString.ToLower()) 
                ||d.Description.ToLower().Contains(searchString.ToLower()) 
                ||d.NameCategory.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.NameVariant).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.NameVariant).ToList();
                    break;

            }
            return View(PaginatedList<VMTblVariant>.CreateAsync(datas, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<IActionResult> Create() 
        {
            VMTblVariant data = new VMTblVariant();
            List<TblCategory> listCategory = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategory;
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(VMTblVariant dataParam) 
        {
            VMResponse response = await variantService.Create(dataParam);

            if (response.Success)
                return Json(new { dataRespon = response });

            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id) 
        {
            VMTblVariant data = await variantService.GetDataById(id);
            List<TblCategory> listCategory = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategory;
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(VMTblVariant dataParam)
        {
            VMResponse response = await variantService.Edit(dataParam);
            dataParam.UpdateBy = idUser;
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            VMTblVariant data = await variantService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id)
        {
            VMResponse response = await variantService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            VMTblVariant data = await variantService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<IActionResult> Restore(int id) 
        {
            VMTblVariant data = await variantService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Restored(int id)
        {
            VMResponse response = await variantService.Restore(id);
            if (response.Success)
                return RedirectToAction("Index");
            return RedirectToAction("Index",id);
        }
    }
}
