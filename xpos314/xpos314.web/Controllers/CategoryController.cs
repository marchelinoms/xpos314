﻿using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;
using xpos314.web.Services;

namespace xpos314.web.Controllers
{
    public class CategoryController : Controller
    {
        private CategoryService categoryService;
        private int idUser = 1;

        public CategoryController(CategoryService _categoryService) 
        {
            this.categoryService = _categoryService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
                pageNumber = 1;
            else
                searchString = currentFilter;

            ViewBag.CurrentFilter = searchString;

            List<TblCategory> datas = await categoryService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            { 
                datas = datas.Where(d => d.NameCategory.ToLower().Contains(searchString.ToLower()) ||
                d.Description.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder) 
            {
                case "name_desc":
                    datas = datas.OrderByDescending(d => d.NameCategory).ToList();
                    break;

                default:
                    datas = datas.OrderBy(d => d.NameCategory).ToList();
                    break;

            }

            return View(PaginatedList<TblCategory>.CreateAsync(datas,pageNumber ?? 1, pageSize ?? 3));
        }
        public IActionResult Create() 
        {
            TblCategory data = new TblCategory();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(TblCategory dataParam) 
        {
            dataParam.CreateBy = idUser;
            VMResponse response = await categoryService.Create(dataParam);
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<JsonResult> CheckNameIsExist(string nameCategory) 
        {
            bool isExist = await categoryService.CheckCategoryByName(nameCategory);
            return Json(isExist);
        }
        public async Task<IActionResult> Edit(int id) 
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(TblCategory dataParam) 
        {
            dataParam.UpdateBy = idUser;
            VMResponse response = await categoryService.Edit(dataParam);
            if (response.Success)
                return Json(new { dataRespon = response });
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id) 
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Deleted(int id) 
        {
            VMResponse response = await categoryService.Delete(id);
            if (response.Success)
                return RedirectToAction("Index");

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id) 
        {
            TblCategory data = await categoryService.GetDataById(id);
            return PartialView(data);
        }

    }
}
