﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class ProductKalbeService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public ProductKalbeService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<ProductKalbe>> GetAllData()
        {
            List<ProductKalbe> datas = new List<ProductKalbe>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProductKalbe/GetAllData");

            datas = JsonConvert.DeserializeObject<List<ProductKalbe>>(apiResponse)!;

            return datas;
        }
        public async Task<bool> CheckProductByName(string productName)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProductKalbe/CheckProductByName/{productName}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }
        public async Task<ProductKalbe> GetDataById(int id)
        {
            ProductKalbe data = new ProductKalbe();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProductKalbe/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<ProductKalbe>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(ProductKalbe dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiProductKalbe/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Edit(ProductKalbe dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiProductKalbe/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiProductKalbe/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }
    }
}
