﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();
        public ProductService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblProduct>> GetAllData()
        {
            List<VMTblProduct> datas = new List<VMTblProduct>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProduct/GetAllData");

            datas = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiResponse)!;

            return datas;
        }
        public async Task<VMTblProduct> GetDataById(int id)
        {
            VMTblProduct data = new VMTblProduct();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiProduct/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProduct>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMTblProduct dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiProduct/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Edit(VMTblProduct dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiProduct/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiProduct/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }

    }
}
