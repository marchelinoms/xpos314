﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class VariantTryService
    {
        private readonly XPOS_314Context db;
        int idUser = 1;
        VMResponse response = new VMResponse();
        public VariantTryService(XPOS_314Context _db) 
        {
            this.db = _db;
        }
        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariant, VMTblVariant>();
                cfg.CreateMap<VMTblVariant, TblVariant>();
            });
            IMapper mapper = config.CreateMapper();
            return mapper;
        }
        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> dataView =
                (
                   from v in db.TblVariants
                   join c in db.TblCategories on v.IdCategory equals c.Id
                   where v.IsDelete == false
                   select new VMTblVariant
                   {
                       Id = v.Id,
                       NameVariant = v.NameVariant,
                       Description = v.Description,

                       IdCategory = v.IdCategory,
                       NameCategory = c.NameCategory
                   }
                ).ToList();

            return dataView;
        }
        public VMResponse Create(VMTblVariant dataView)
        {
            TblVariant dataModel = GetMapper().Map<TblVariant>(dataView);
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;
            dataModel.IsDelete = false;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                response.Message = "Data has been successfuly created";
                response.Entity = dataModel;
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Data has not been created" + e.Message;
                response.Entity = dataView;
            }
            return response;
        }
        public VMResponse Edit(VMTblVariant dataView) 
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;

            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Data has been edited";
                response.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Failed to Edit the data" + e.Message;
                response.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }

            return response;
        }
        public VMTblVariant GetById(int id)
        {
            VMTblVariant dataView = (
                   from v in db.TblVariants
                   join c in db.TblCategories on v.IdCategory equals c.Id
                   where v.IsDelete == false && v.Id == id
                   select new VMTblVariant
                   {
                       Id = id,
                       NameVariant = v.NameVariant,
                       Description = v.Description,
                       CreateBy = v.CreateBy,
                       CreateDate = v.CreateDate,
                       UpdateBy = v.UpdateBy,
                       UpdateDate = v.UpdateDate,
                       IdCategory = v.IdCategory,
                       NameCategory = c.NameCategory
                   }
                ).FirstOrDefault()!;
            return dataView;
        }
        
        public VMResponse Delete(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();
                response.Message = "Data has been deleted";
                response.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Delete failed" + e.Message;
                response.Entity = GetMapper().Map<VMTblVariant>(dataModel);
            }

            return response;
        }
    }
}
