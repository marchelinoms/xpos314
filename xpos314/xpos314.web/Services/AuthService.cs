﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public AuthService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<VMTblCustomer> CheckLogin(string email, string password) 
        {
            VMTblCustomer customer =  new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");
            customer = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse)!;

            return customer;
        }
        public async Task<List<VMMenuAccess>> MenuAccess(int IdRole) 
        {
            List<VMMenuAccess> data = new List<VMMenuAccess>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMenuAccess>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> CreateCustomer(TblCustomer dataParam) 
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + $"apiAuth/CreateCustomer", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else 
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<bool> CheckEmailExisting(string email) 
        {
            bool data = false;
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckEmailExisting/{email}");

            return data;
        }
    }
}
