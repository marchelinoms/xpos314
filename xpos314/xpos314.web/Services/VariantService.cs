﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
	public class VariantService
	{
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();
        public VariantService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblVariant>> GetAllData()
        {
            List<VMTblVariant> datas = new List<VMTblVariant>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiVariant/GetAllData");

            datas = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse)!;

            return datas;
        }
        public async Task<List<VMTblVariant>> GetDataByIdCategory(int id) 
        {
            List<VMTblVariant> data = new List<VMTblVariant>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataByIdCategory/{id}");
            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse)!;

            return data;

        }
        public async Task<VMTblVariant> GetDataById(int id)
        {
            VMTblVariant data = new VMTblVariant();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiVariant/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblVariant>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMTblVariant dataParam) 
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiVariant/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Edit(VMTblVariant dataParam) 
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiVariant/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiVariant/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }
        public async Task<VMResponse> Restore(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiVariant/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }

    }
}
