﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CategoryService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public CategoryService(IConfiguration _configuration) 
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];       
        }
        public async Task<List<TblCategory>> GetAllData() 
        {
            List<TblCategory> datas = new List<TblCategory>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCategory/GetAllData");

            datas = JsonConvert.DeserializeObject<List<TblCategory>>(apiResponse)!;

            return datas;
        }
        public async Task<VMResponse> Create(TblCategory dataParam) 
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiCategory/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else 
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<bool> CheckCategoryByName(string nameCategory) 
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/CheckCategoryByName/{nameCategory}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }
        public async Task<TblCategory> GetDataById(int id) 
        {
            TblCategory data = new TblCategory();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategory/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<TblCategory>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Edit(TblCategory dataParam) 
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiCategory/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else 
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id) 
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategory/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else 
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }

    }
}
