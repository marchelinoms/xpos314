﻿using AutoMapper;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CategoryTryService
    {
        private readonly XPOS_314Context db;
        int idUser = 1;
        VMResponse response = new VMResponse();
        public CategoryTryService(XPOS_314Context _db) 
        {
            this.db = _db;
        }
        public static IMapper GetMapper() 
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCategory, VMTblCategory>();
                cfg.CreateMap<VMTblCategory, TblCategory>();
            });
            IMapper mapper = config.CreateMapper();
            return mapper;
        }
        public List<VMTblCategory> GetAllData() 
        {
            List<TblCategory> dataModel = db.TblCategories.Where(i => i.IsDelete == false).ToList();
            List<VMTblCategory> dataView = GetMapper().Map<List<VMTblCategory>>(dataModel);

            return dataView;
        }
        public VMResponse Create(VMTblCategory dataView) 
        {
            TblCategory dataModel = GetMapper().Map<TblCategory>(dataView);
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;
            dataModel.IsDelete = false;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                response.Message="Data has been successfuly created";
                response.Entity = dataModel;
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Data has not been created" + e.Message;
                response.Entity = dataView; 
            }
            return response;
        }
        public VMTblCategory GetId(int id) 
        {
            TblCategory dataModel = db.TblCategories.Find(id)!;
            VMTblCategory dataView = GetMapper().Map<VMTblCategory>(dataModel);
            return dataView;
        }
        public VMResponse Edit(VMTblCategory dataView) { 
            TblCategory dataModel = db.TblCategories.Find(dataView.Id)!;
            dataModel.NameCategory = dataView.NameCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Data successfuly edited";
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Editing data failed" + e.Message;
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            return response;
        }
        public VMResponse Delete(VMTblCategory dataView) 
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id)!;
            dataModel.IsDelete = true;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();
                response.Message = "Data has been deleted";
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch (Exception e)
            {
                response.Success =  false;
                response.Message = "Delete failed" + e.Message;
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }

            return response;
        }

    }
}
