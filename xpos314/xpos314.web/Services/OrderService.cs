﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
	public class OrderService
	{
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();
        public OrderService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<VMResponse> SubmitOrder(VMOrderHeader dataHeader) 
        {
            string json = JsonConvert.SerializeObject(dataHeader);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiOrder/SubmitOrder" ,content);
            VMResponse response = new VMResponse();

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else 
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<List<VMOrderHeader>> ListHeaderDetails(int IdCustomer) 
        {
            var apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/GetDataOrderHeaderDetail/{IdCustomer}");
            List<VMOrderHeader> listHeader = JsonConvert.DeserializeObject<List<VMOrderHeader>>(apiResponse)!;

            return listHeader;
        }
        public async Task<int> CountTransaction(int IdCustomer) 
        {

            var apiResponse = await client.GetStringAsync(RouteAPI + $"apiOrder/CountTransaction/{IdCustomer}");
            int count = JsonConvert.DeserializeObject<int>(apiResponse);

            return count;
        }
    }
}
