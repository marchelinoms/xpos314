﻿using Newtonsoft.Json;
using System.Text;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
	public class CustomerService
	{
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public CustomerService(IConfiguration _configuration )
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMTblCustomer>> GetAllData()
        {
            List<VMTblCustomer> datas = new List<VMTblCustomer>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomer/GetAllData");

            datas = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse)!;

            return datas;
        }
        public async Task<List<VMTblCustomer>> GetDataByIdRole(int id)
        {
            List<VMTblCustomer> data = new List<VMTblCustomer>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataByIdRole/{id}");
            data = JsonConvert.DeserializeObject<List<VMTblCustomer>>(apiResponse)!;

            return data;

        }
        public async Task<VMTblCustomer> GetDataById(int id)
        {
            VMTblCustomer data = new VMTblCustomer();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomer/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblCustomer>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMTblCustomer dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiCustomer/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Edit(VMTblCustomer dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiCustomer/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }
        public async Task<VMResponse> Restore(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomer/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }
    }

}
