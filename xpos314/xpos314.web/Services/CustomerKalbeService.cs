﻿using Newtonsoft.Json;
using System.Text;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.web.Services
{
    public class CustomerKalbeService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse response = new VMResponse();

        public CustomerKalbeService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<CustomerKalbe>> GetAllData()
        {
            List<CustomerKalbe> datas = new List<CustomerKalbe>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCustomerKalbe/GetAllData");

            datas = JsonConvert.DeserializeObject<List<CustomerKalbe>>(apiResponse)!;

            return datas;
        }
        public async Task<bool> CheckProductByName(string productName)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomerKalbe/CheckProductByName/{productName}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }
        public async Task<CustomerKalbe> GetDataById(int id)
        {
            CustomerKalbe data = new CustomerKalbe();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCustomerKalbe/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<CustomerKalbe>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(CustomerKalbe dataParam)
        {
            //Convert Object to String
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "apiCustomerKalbe/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Edit(CustomerKalbe dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PutAsync(RouteAPI + "apiCustomerKalbe/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }

            return response;
        }
        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCustomerKalbe/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiResponse = await request.Content.ReadAsStringAsync();
                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse)!;
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode}:{request.ReasonPhrase}";
            }
            return response;
        }
    }
}
