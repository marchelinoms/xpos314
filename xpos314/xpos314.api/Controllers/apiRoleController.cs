﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiRoleController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int idUser = 1;
        public apiRoleController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblRole> GetAllData() 
        {
            List<TblRole> data = db.TblRoles.ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public TblRole GetDataById(int id) 
        {
            TblRole data = db.TblRoles.Where(d => d.Id == id).FirstOrDefault()!;
            return data;
        }
        [HttpGet("CheckRoleByName/{name}")]
        public bool CheckName(string name)
        {
            TblRole data = db.TblRoles.Where(n => n.RoleName == name && n.IsDelete == false).FirstOrDefault()!;
            if (data != null)
                return true;
            else
                return false;
        }

        [HttpPost("Save")]
        public VMResponse Create(TblRole data)
        {
            data.CreateBy = idUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;
            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblRole data)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (data != null)
            {
                dt.RoleName = data.RoleName;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblRole dt = db.TblRoles.Where(a => a.Id == id).FirstOrDefault()!;
            bool available = dt.IsDelete == false ? true : false;
            if (dt != null)
            {
                dt.IsDelete = available;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = $"Data {dt.RoleName} has been deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
