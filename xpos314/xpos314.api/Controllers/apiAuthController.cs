﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiAuthController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int IdUser = 1;
        public apiAuthController(XPOS_314Context db)
        {
            this.db = db;
        }

        [HttpGet("CheckLogin/{email}/{password}")]
        public VMTblCustomer CheckLogin(string email, string password) 
        {
            VMTblCustomer data = (from c in db.TblCustomers
                                  join r in db.TblRoles on c.IdRole equals r.Id
                                  where c.IsDelete == false && c.Email == email && c.Password == password
                                  select new VMTblCustomer 
                                  {
                                      Id = c.Id,
                                      NameCustomer = c.NameCustomer,
                                      IdRole = c.IdRole,
                                      RoleName = r.RoleName
                                  }).FirstOrDefault()!;
            return data;
        }
        [HttpGet("MenuAccess/{IdRole}")]
        public List<VMMenuAccess> MenuAccess(int IdRole)
        {
            List<VMMenuAccess> listMenu = new List<VMMenuAccess>();

            listMenu = (from parent in db.TblMenus
                        join ma in db.TblMenuAccesses
                        on parent.Id equals ma.MenuId
                        where parent.IsParent == true && ma.IdRole == IdRole
                        select new VMMenuAccess
                        {
                            Id = parent.Id,
                            MenuName = parent.MenuName,
                            MenuIcon = parent.MenuIcon,
                            IdRole = ma.IdRole,
                            MenuSorting = parent.MenuSorting,
                            ListChild = (from child in db.TblMenus
                                         where child.MenuParent == parent.Id
                                         select new VMMenuAccess
                                         {
                                             Id = child.Id,
                                             MenuName = child.MenuName,
                                             MenuAction = child.MenuAction,
                                             MenuController = child.MenuController,
                                             MenuIcon = child.MenuIcon,
                                             MenuParent = child.MenuParent,
                                             MenuSorting = child.MenuSorting
                                         }).OrderBy(a => a.MenuSorting).ToList()
                        }).OrderBy(a => a.MenuSorting).ToList();

            return listMenu;
        }
        [HttpPost("CreateCustomer")]
        public VMResponse CreateCustomer(TblCustomer data) 
        {
            data.CreateBy = IdUser;
            data.CreateDate = DateTime.Now;

            try
            {
                db.Add(data);
                db.SaveChanges();

                response.Message = $"Registration Complete and Success, Welcome {data.NameCustomer}";
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Registration Failed: " + e.Message;
            }

            return response;
        }
        [HttpGet("CheckEmailExisting/{email}")]
        public bool CheckEmailExisting(string email) 
        {
            TblCustomer customer = db.TblCustomers.Where(c => c.Email == email).FirstOrDefault()!;
            if(customer != null)
                return true;
            else
                return false;
        }
    }
}
