﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int idUser = 1;

        public apiProductController(XPOS_314Context _db)
        {
            this.db = _db;
        }
        [HttpGet("GetAllData")]
        public List<VMTblProduct> GetAllData() 
        {
            List<VMTblProduct> data = (from p in db.TblProducts
                                       join v in db.TblVariants on p.IdVariant equals v.Id
                                       join c in db.TblCategories on v.IdCategory equals c.Id
                                       where p.IsDelete == false
                                       select new VMTblProduct 
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           Image = p.Image,
                                           
                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory
                                           
                                       }).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblProduct GetDataById(int id) 
        {
            VMTblProduct data = (from p in db.TblProducts
                                       join v in db.TblVariants on p.IdVariant equals v.Id
                                       join c in db.TblCategories on v.IdCategory equals c.Id
                                       where p.IsDelete == false && p.Id == id
                                       select new VMTblProduct 
                                       {
                                           Id = p.Id,
                                           NameProduct = p.NameProduct,
                                           Price = p.Price,
                                           Stock = p.Stock,
                                           Image = p.Image,
                                           
                                           IdVariant = p.IdVariant,
                                           NameVariant = v.NameVariant,

                                           IdCategory = v.IdCategory,
                                           NameCategory = c.NameCategory
                                           
                                       }).FirstOrDefault()!;
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Create(TblProduct data)
        {
            data.CreateBy = idUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblProduct data)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (data != null)
            {
                dt.NameProduct = data.NameProduct;
                dt.Price = data.Price;
                dt.Stock = data.Stock;
                if (data.Image != null) 
                { 
                    dt.Image = data.Image;
                }
                dt.IdVariant = data.IdVariant;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblProduct dt = db.TblProducts.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = $"Data {dt.NameProduct} has been deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
