﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiVariantController : ControllerBase
	{
		private readonly XPOS_314Context db;
		private VMResponse response = new VMResponse();
		private int idUser = 1;

		public apiVariantController(XPOS_314Context _db) 
		{
			this.db = _db;
		}
		[HttpGet("GetAllData")]
		public List<VMTblVariant> GetAllData() 
		{
			List<VMTblVariant> data = (from v in db.TblVariants
									   join c in db.TblCategories on v.IdCategory equals c.Id
									   //where v.IsDelete == false
									   select new VMTblVariant 
									   {
										   Id = v.Id,
										   NameVariant = v.NameVariant,
										   Description = v.Description,
										   IdCategory = v.IdCategory,
                                           CreateDate = v.CreateDate,
                                           IsDelete = v.IsDelete,
										   NameCategory = c.NameCategory
									   }).ToList();
			return data;
		}

		[HttpGet("GetDataById/{id}")]
		public VMTblVariant GetDataById(int id) 
		{
			VMTblVariant data = (from v in db.TblVariants
                                 join c in db.TblCategories on v.IdCategory equals c.Id
                                 where v.Id == id
                                 select new VMTblVariant
                                 {
                                     Id = v.Id,
                                     NameVariant = v.NameVariant,
                                     Description = v.Description,
                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory
                                 }).FirstOrDefault()!;
			return data;
		}

		[HttpGet("GetDataByIdCategory/{id}")]
        public List<VMTblVariant> GetDataByIdCategory(int id)
        {
           List<VMTblVariant> data = (from v in db.TblVariants
                                 join c in db.TblCategories on v.IdCategory equals c.Id
                                 where v.IsDelete == false && v.IdCategory == id
                                 select new VMTblVariant
                                 {
                                     Id = v.Id,
                                     NameVariant = v.NameVariant,
                                     Description = v.Description,
                                     IdCategory = v.IdCategory,
                                     NameCategory = c.NameCategory
                                 }).ToList();
            return data;
        }

		[HttpPost("Save")]
		public VMResponse Create(TblVariant data) 
		{
			data.CreateBy = idUser;
			data.CreateDate = DateTime.Now;
			data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblVariant data)
        {
            TblVariant dt = db.TblVariants.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (data != null)
            {
                dt.NameVariant = data.NameVariant;
                dt.Description = data.Description;
                dt.IdCategory =  data.IdCategory; 
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TblVariant dt = db.TblVariants.Where(a => a.Id == id).FirstOrDefault()!;
            bool available = dt.IsDelete == false ? true : false;
            if (dt != null)
            {
                dt.IsDelete = available;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = $"Data {dt.NameVariant} has been deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed : " + e.Message;
                }
            }
            else
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
