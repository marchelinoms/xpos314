﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int idUser = 1;

        public apiCategoryController(XPOS_314Context _db) 
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<TblCategory> GetAllData()
        {
            List<TblCategory> datas = db.TblCategories.Where(d => d.IsDelete == false).ToList();

            return datas;
        }

        [HttpGet("GetDataById/{id}")]
        public TblCategory DataById(int id) 
        {
            TblCategory result = new TblCategory();
            result = db.TblCategories.Where(r => r.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}")]
        public bool CheckName(string name) 
        {
            TblCategory data = db.TblCategories.Where(n => n.NameCategory == name && n.IsDelete == false).FirstOrDefault()!;
            if (data != null)
                return true;
            else
                return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(TblCategory data) 
        {
            data.CreateBy = idUser;
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(TblCategory data) 
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == data.Id).FirstOrDefault()!;
            if (data != null)
            {
                dt.NameCategory = data.NameCategory;
                dt.Description = data.Description;
                dt.UpdateBy = idUser;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpGet("Delete/{id}/{createBy}")]
        public VMResponse Delete(int id, int createBy) 
        {
            TblCategory dt = db.TblCategories.Where(a => a.Id == id).FirstOrDefault()!;
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.UpdateBy = createBy;
                dt.UpdateDate = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = $"Data {dt.NameCategory} has been deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
