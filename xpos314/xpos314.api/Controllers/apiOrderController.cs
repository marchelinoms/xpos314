﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Globalization;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiOrderController : Controller
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();
        private int idUser = 1;

        public apiOrderController(XPOS_314Context _db)
        {
            this.db = _db;
        }

        [HttpGet("GetDataOrderHeader")]
        public List<TblOrderHeader> GetDataHeader()
        {
            List<TblOrderHeader> data = db.TblOrderHeaders.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataOrderDetail/{id}")]
        public List<VMOrderDetail> GetDataOrderDetail(int id)
        {
            List<VMOrderDetail> data = (from d in db.TblOrderDetails
                                        join p in db.TblProducts on d.IdProduct equals p.Id
                                        where d.IsDelete == false && d.IdHeader == id
                                        select new VMOrderDetail
                                        {
                                            Id = d.Id,
                                            Qty = d.Qty,
                                            SumPrice = d.SumPrice,

                                            IdProduct = d.IdProduct,
                                            NameProduct = p.NameProduct,
                                            Price = p.Price
                                        }).ToList();
            return data;
        }

        [HttpPost("SubmitOrder")]
        public VMResponse SubmitOrder(VMOrderHeader dataHeader)
        {
            TblOrderHeader head = new TblOrderHeader();
            head.CodeTransaction = GenerateCode();
            head.Amount = dataHeader.Amount;
            head.TotalQty = dataHeader.TotalQty;
            head.IdCustomer = idUser;
            head.IsCheckOut = true;
            head.IsDelete = false;
            head.CreateDate = DateTime.Now;
            head.CreateBy = idUser;

            try
            {
                db.Add(head);
                db.SaveChanges();

                foreach (VMOrderDetail item in dataHeader.ListDetails)
                {
                    TblOrderDetail detail = new TblOrderDetail();
                    detail.IdHeader = head.Id;
                    detail.IdProduct = item.IdProduct;
                    detail.Qty = item.Qty;
                    detail.SumPrice = item.SumPrice;
                    detail.IsDelete = false;
                    detail.CreateBy = idUser;
                    detail.CreateDate = DateTime.Now;
                    try
                    {
                        db.Add(detail);
                        db.SaveChanges();

                        TblProduct dataProduct = db.TblProducts.Where(p => p.Id == item.IdProduct).FirstOrDefault()!;
                        if (dataProduct != null)
                        {
                            dataProduct.Stock -= item.Qty;
                            db.Update(dataProduct);
                            db.SaveChanges();
                        }

                        response.Message = "Your order has been proccessed, thank you for your order ";
                    }
                    catch (Exception e)
                    {
                        response.Success = false;
                        response.Message = "Save failed :" + e.Message;
                    }
                }
            }
            catch (Exception e)
            {
                response.Success = false;
                response.Message = "Save failed :" + e.Message;
            }
            return response;
        }

       [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"XPOS-{DateTime.Now.ToString("yyyyMMdd")}-";
            string digit = "";

            TblOrderHeader data = db.TblOrderHeaders.OrderByDescending(a => a.CodeTransaction).FirstOrDefault()!;
            if (data != null)
            {
                string codeTrf = data.CodeTransaction;
                string[] codeSplit = codeTrf.Split('-');
                int lastdigit = int.Parse(codeSplit[2]) + 1;
                digit = lastdigit.ToString("00000");
            }
            else
            {
                digit = "00001";
            }

            return code + digit;
        }
        [HttpGet("CountTransaction/{IdCustomer}")]
        public int CountTransaction(int IdCustomer)
        {
            int count = 0;
            count = db.TblOrderHeaders.Where(a => a.IdCustomer == IdCustomer && a.IsDelete == false).Count();
            return count;
        }
        [HttpGet("GetDataOrderHeaderDetail/{IdCustomer}")]
        public List<VMOrderHeader> GetDataOrderHeaderDetail(int IdCustomer) 
        {
            DateTime dt = DateTime.Now;
            var hari = dt.ToString("dddd", new CultureInfo("id-ID"));
            //var jam = dt.Hour;
            //var menit = dt.Minute;
            List<VMOrderHeader> data = (from h in db.TblOrderHeaders
                                        where h.IsDelete == false && h.IdCustomer == IdCustomer
                                        select new VMOrderHeader
                                        {
                                            Id = h.Id,
                                            Amount = h.Amount,
                                            TotalQty = h.TotalQty,
                                            IsCheckOut = h.IsCheckOut,
                                            IdCustomer = h.IdCustomer,
                                            CodeTransaction = h.CodeTransaction,
                                            CreateDate = h.CreateDate,
                                            ListDetails = (from d in db.TblOrderDetails
                                                           join p in db.TblProducts on d.IdProduct equals p.Id
                                                           where d.IsDelete == false && d.IdHeader == h.Id
                                                           select new VMOrderDetail
                                                           {
                                                               Id = d.Id,
                                                               Qty = d.Qty,
                                                               SumPrice = d.SumPrice,
                                                               IdHeader = d.IdHeader,
                                                               IdProduct = d.IdProduct,
                                                               NameProduct = p.NameProduct,
                                                               Price = p.Price
                                                           }).ToList(),
                                            //online = h.Id == 1 && int.Parse(h.CodeTransaction.Substring(0,2)) > jam 
                                            //    && int.Parse(h.CodeTransaction.Substring(0,2)) > menit ? true : false
                                        }).ToList();
            return data;
        }
    }
}
