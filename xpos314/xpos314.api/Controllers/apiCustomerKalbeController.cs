﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCustomerKalbeController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();

        public apiCustomerKalbeController(XPOS_314Context _db) 
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<CustomerKalbe> GetAllData()
        {
            List<CustomerKalbe> datas = db.CustomerKalbes.ToList();

            return datas;
        }

        [HttpGet("GetDataById/{id}")]
        public CustomerKalbe DataById(int id) 
        {
            CustomerKalbe result = db.CustomerKalbes.Where(r => r.CustomerId == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCustomerByName/{customerName}")]
        public bool CheckName(string customerName) 
        {
            CustomerKalbe data = db.CustomerKalbes.Where(n => n.CustomerName == customerName && n.IsDelete == false).FirstOrDefault()!;
            if (data != null)
                return true;
            else
                return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(CustomerKalbe data) 
        {
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(CustomerKalbe data) 
        {
            CustomerKalbe dt = db.CustomerKalbes.Where(a => a.CustomerId == data.CustomerId).FirstOrDefault()!;
            if (data != null)
            {
                dt.CustomerName = data.CustomerName;
                dt.CustomerAddress = data.CustomerAddress;
                dt.Gender = data.Gender;
                dt.BirthDate = data.BirthDate;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpGet("Delete/{id}")]
        public VMResponse Delete(int id) 
        {
            CustomerKalbe dt = db.CustomerKalbes.Where(a => a.CustomerId == id).FirstOrDefault()!;
            bool available = dt.IsDelete == false ? true : false;
            if (dt != null)
            {
                dt.IsDelete = available;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = $"Data {dt.CustomerName} has been deleted";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete Failed : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
