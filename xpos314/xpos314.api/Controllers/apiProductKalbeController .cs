﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using xpos314.datamodels;
using xpos314.viewmodels;

namespace xpos314.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiProductKalbeController : ControllerBase
    {
        private readonly XPOS_314Context db;
        private VMResponse response = new VMResponse();

        public apiProductKalbeController(XPOS_314Context _db) 
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<ProductKalbe> GetAllData()
        {
            List<ProductKalbe> datas = db.ProductKalbes.ToList();

            return datas;
        }

        [HttpGet("GetDataById/{id}")]
        public ProductKalbe DataById(int id) 
        {
            ProductKalbe result = new ProductKalbe();
            result = db.ProductKalbes.Where(r => r.ProductId == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckProductByName/{productName}")]
        public bool CheckName(string productName) 
        {
            ProductKalbe data = db.ProductKalbes.Where(n => n.ProductName == productName && n.IsDelete == false).FirstOrDefault()!;
            if (data != null)
                return true;
            else
                return false;
        }
        [HttpGet("GenerateCode")]
        public string GenerateCode()
        {
            string code = $"P";
            string digit = "";

            ProductKalbe data = db.ProductKalbes.OrderByDescending(a => a.ProductCode).FirstOrDefault()!;
            if (data != null)
            {
                string codeTrf = data.ProductCode;
                string[] codeSplit = codeTrf.Split('P');
                int lastdigit = int.Parse(codeSplit[1]) + 1;
                digit = lastdigit.ToString("0000");
            }
            else
            {
                digit = "0001";
            }

            return code + digit;
        }
        [HttpPost("Save")]
        public VMResponse Save(ProductKalbe data) 
        {
            data.ProductCode = GenerateCode();
            data.CreateDate = DateTime.Now;
            data.IsDelete = false;
            try
            {
                db.Add(data);
                db.SaveChanges();
                response.Message = "Data has been saved";
            }
            catch (Exception e) 
            {
                response.Success = false;
                response.Message = "Failed to save :" + e.Message;
            }
            return response;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(ProductKalbe data) 
        {
            ProductKalbe dt = db.ProductKalbes.Where(a => a.ProductId == data.ProductId).FirstOrDefault()!;

            if (data != null)
            {
                dt.ProductCode = data.ProductCode;
                dt.ProductName = data.ProductName;
                dt.Price = data.Price;
                dt.Quantity = data.Quantity;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    response.Message = "Data has been updated";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Update Fail : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id) 
        {
            ProductKalbe dt = db.ProductKalbes.Where(a => a.ProductId == id).FirstOrDefault()!;
            bool available = dt.IsDelete == false ? true : false;
            if (dt != null)
            {
                dt.IsDelete = available;
                try
                {
                    db.Update(dt);
                    db.SaveChanges();
                    if(dt.IsDelete == true)
                        response.Message = $"Data {dt.ProductName} has been deleted";
                    else
                        response.Message = $"Data {dt.ProductName} has been restored";
                }
                catch (Exception e)
                {
                    response.Success = false;
                    response.Message = "Delete/Restore Failed : " + e.Message;
                }
            }
            else 
            {
                response.Success = false;
                response.Message = "Data not found";
            }
            return response;
        }
    }
}
