﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace xpos314.viewmodels
{
    public class VMResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public object Entity { get; set; }
        public VMResponse() {
            Success = true;
        }
    }
}
