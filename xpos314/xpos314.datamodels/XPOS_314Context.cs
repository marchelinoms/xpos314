﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace xpos314.datamodels
{
    public partial class XPOS_314Context : DbContext
    {
        public XPOS_314Context()
        {
        }

        public XPOS_314Context(DbContextOptions<XPOS_314Context> options)
            : base(options)
        {
        }

        public virtual DbSet<CustomerKalbe> CustomerKalbes { get; set; } = null!;
        public virtual DbSet<PenjualanKalbe> PenjualanKalbes { get; set; } = null!;
        public virtual DbSet<ProductKalbe> ProductKalbes { get; set; } = null!;
        public virtual DbSet<TblCategory> TblCategories { get; set; } = null!;
        public virtual DbSet<TblCustomer> TblCustomers { get; set; } = null!;
        public virtual DbSet<TblMenu> TblMenus { get; set; } = null!;
        public virtual DbSet<TblMenuAccess> TblMenuAccesses { get; set; } = null!;
        public virtual DbSet<TblOrderDetail> TblOrderDetails { get; set; } = null!;
        public virtual DbSet<TblOrderHeader> TblOrderHeaders { get; set; } = null!;
        public virtual DbSet<TblProduct> TblProducts { get; set; } = null!;
        public virtual DbSet<TblRole> TblRoles { get; set; } = null!;
        public virtual DbSet<TblVariant> TblVariants { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost;Initial Catalog=XPOS_314;user id=Marchel;Password=marsoit1234");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerKalbe>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PK__Customer__A4AE64D8C25B9C5D");
            });

            modelBuilder.Entity<PenjualanKalbe>(entity =>
            {
                entity.HasKey(e => e.SalesOrderId)
                    .HasName("PK__Penjuala__B14003E23B7D1AA8");
            });

            modelBuilder.Entity<ProductKalbe>(entity =>
            {
                entity.HasKey(e => e.ProductId)
                    .HasName("PK__ProductK__B40CC6CD0D0D18AE");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
