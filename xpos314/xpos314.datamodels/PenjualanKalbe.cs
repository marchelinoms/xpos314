﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("PenjualanKalbe")]
    public partial class PenjualanKalbe
    {
        [Key]
        public int SalesOrderId { get; set; }
        [Unicode(false)]
        public string CustomerName { get; set; } = null!;
        [Unicode(false)]
        public string ProductName { get; set; } = null!;
        [Column(TypeName = "datetime")]
        public DateTime DateOrder { get; set; }
        public int Quantity { get; set; }
        public bool IsDelete { get; set; }
    }
}
