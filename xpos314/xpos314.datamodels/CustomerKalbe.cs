﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("CustomerKalbe")]
    public partial class CustomerKalbe
    {
        [Key]
        public int CustomerId { get; set; }
        [Unicode(false)]
        public string CustomerName { get; set; } = null!;
        [Unicode(false)]
        public string CustomerAddress { get; set; } = null!;
        public bool Gender { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime BirthDate { get; set; }
        public bool IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
    }
}
