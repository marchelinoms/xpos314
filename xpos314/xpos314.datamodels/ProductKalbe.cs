﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace xpos314.datamodels
{
    [Table("ProductKalbe")]
    public partial class ProductKalbe
    {
        [Key]
        public int ProductId { get; set; }
        [Unicode(false)]
        public string ProductCode { get; set; } = null!;
        [Unicode(false)]
        public string ProductName { get; set; } = null!;
        public int Quantity { get; set; }
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }
        public bool IsDelete { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime CreateDate { get; set; }
    }
}
